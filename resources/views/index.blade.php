<!-- inicio -->
@extends ('template/base')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/portafolio1.jpg')}}" class='header-img' style=''>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
            
            <div class="maintext">
                <div class="primary-text inviewport animated delay4" data-effect="fadeInRightBig">
                    <div class="left-line">
                        <h4>Bienvenido a mi</h4>
                        <h1>porta</h1>
                    </div>
                </div>
                <div class="black-text inviewport animated delay4" data-effect="fadeInLeftBig">
                    <div>
                        <h1>folio</h1>
                    </div>
                </div>
            </div>
</div>
@endsection
@section ('content')
<div class="row">
<h1 class="heading"> Elige usuario: </h1>
<div id="fa"></div>
<div id="resul"></div>
<div class='form-group'>
    <div class='col-md-6 col-md-offset-3'>
        <div id="0" class="im " style="border: solid 12px ;border-radius: 20px ;display:block" height="auto"> 
            <img  src="{{asset('img/usergif.gif')}}" class='img-responsive center-block'  style="background-size: contain; height: 250px; width: 100%">
        </div>
        @foreach($usuarios as $k=>$uu)
        <div id="{{$uu->id_user}}" class="im hide" style="border: solid 12px ;border-radius: 20px ;display:block" height="auto" > 
            <img   src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$uu->imagen)}}" class='img-responsive center-block'  style="background-size: contain; height: 250px; width: 100%">
        </div>
        @endforeach
         <br/>    
</div>
        <div class="divPanel page-content">
        <div class="row-fluid">
                    <div class="list_carousel2 responsive">
                        <ul id="list_photos">
                            @if(($usuarios->count()) > 4)
                            @foreach($usuarios as $usi)
                            <li><img title="{{$usi->nombreCompleto}}" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$usi->imagen)}}" height="50px" width="120%"  </li>     
                            @endforeach
                            @endif
                        </ul>
                    </div>

{!! Form::open(['route' => ['indexSelect'], 'method' =>'POST']) !!}
    
    <select class="form-control" name="usuario" onchange='mostrar(this);'>
         <option value="0">lista de usuarios. </option>
        @foreach($usuarios as $usuario)
           <option value="{{$usuario->id_user}}">{{$usuario->nombreCompleto}}, {{$usuario->profesion}} en {{$usuario->zonaGeografica}}.  </option>
         @endforeach     
    </select>
   </div>
            <div class='form-group' >
        <button type="submit" class="btn-lg btn-info center-block" style="margin-top: 5px; width: 100px ">
            <span class='glyphicon glyphicon-book'></span>   
        </button>
    </div>
    
{!! Form::close() !!}

</div>
</div>
<hr>

<div class="row">
<h1 class="heading"> Gracias por visitar esta Web </h1>
<div class="headul"></div>

    <p class="subheading">
        TuPortafolio es una app web en la que podras tener en poco tiempo un sitio en la red para mostrar tu experiencia profesional.
        Desde mostrar digitalmente todo tu trabajo o simplemente un constructor web de CV.
        Mi nombre es Rubén, Soy Programador y creador, realizar proyectos de este tipo porque es algo que siempre me ha atraido irremediablemente.
        Me he dedicado a multitud de oficios a lo largo de mi vida laboral para ganarme la vida, 
        pero un día comencé a perseguir la idea de dedicarme a lo que realmente me gusta,
        sin reparar, dentro de mis posibilidades, en el esfuerzo o el tiempo que cuestase conseguirlo.
        Este Site lo dedico a todo aquel que quiera recopilar su experiencia laboral, 
        con la intención de que sea un portafolio sencillo y vivo, un proyecto a largo plazo y en continua revisión.
        Existe una parte de administración  privada con formularios rápidos para inserción de nuevos contenidos,
        Se persigue no dejar de seguir alimentando la colección del portafolio a menudo, sin ser el CV que se suele enviar para conseguir entrevistas de trabajo, 
        y poder mostrar más bien una trayectoria profesional como un escaparate digital. 
        Es importante tener un sitio donde poder referir a todo aquel que le pueda interesar nuestro trabajo.
        El objetivo es que nunca pueda ser un proyecto definitivo, en tuPortafolio.com el mínimo de datos lo decides tú, es bueno saber autopromocionarse.
        Hoy en día se deben usar todas las herramientas así que aqui se usan los campos de redes sociales para aumentar las posibilidades, 
        y generarar más tráfico. Es buena idea incluir la url de tu portafolio en cualquier solicitud de empleo.
        PF está abierto a sugerencias usando el área de contacto.
        
    </p>
    
</div>

    <script type="text/javascript">
        function mostrar( obj ){
            $('#fa').html("");
        var r = parseInt(obj[ obj.selectedIndex ].value );  
            var f = ($('.im').attr("id"));
            $('div .im').each(function(){
                var ff= ($(this).attr("id"));
                if(ff!=r)
                {
                     $(this).hide(); 
                }
                if(ff == r) 
                {
                    $(this).removeClass("hide"); 
                    $(this).show();
                 //   $(this).show();
                }
            });
        }
        
      var token = $('meta[name="csrf-token"]').attr('content');
      $(document).ready(function(){      
          $.ajaxSetup({
              headers: { 'X-CSRF-TOKEN': token }
          });
          
      });
      
   $('#list_photos').carouFredSel({ responsive: true, width: '92%', scroll: 1, items: {width: 220,visible: {min: 2, max: 6}} });</script>
@endsection
