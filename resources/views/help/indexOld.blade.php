<!DOCTYPE html>
<html>
    <head>
        <title>Portfolio ruben</title>
        <meta charset="utf-8"/>
        <meta name=viewport content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="js/pace.min.js"></script>
        <link href="pace-loading-bar.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/animate.caliber.css">
        <link rel="stylesheet" type="text/css" href="css/materialdesignicons.caliber.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/inviewport-1.3.2.js"></script>
        <!--Mixitup -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
        <!--Fancybox -->
        <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />
        <!-- Main Style -->
        <link rel="stylesheet" id="main-style" type="text/css" href="css/style.css">
        <script type="text/javascript" src="js/main.js"></script>
    </head>
    <body class=" angled  yellow">
        
        <!-- Section Start - Header -->
        <section class="header bg-lightgray header-1" >
            <!-- Menu Top Bar - Start -->
            <div class="topbar " data-effect="fadeIn">
                <div class="menu">
                    <div class="primary inviewport animated delay4" data-effect="fadeInRightBig">
                        <div class='cssmenu'>
                            <!-- Menu - Start -->
                            <ul class='menu-ul'>
                                <li class='has-sub'>    
                                    <a href='index.php'>INICIO </a>
                                </li>
                                <li class='has-sub'>
                                    <a href='contacto.php'>CONTACTO</a>
                                </li>
                                <li class='has-sub'>
                                    <a href='blog.php'>BLOG</a>
                                </li>
                                <li class='has-sub'>
                                    <a href='#'>WEBS<i class='mdi mdi-chevron-down'></i></a>
                                        <ul>
                                            <li>
                                                <a href='miworspress.html'>WORDPRESS</a>
                                            </li>
                                            <li>
                                                <a href='mipresta.html'>PRESTASHOP</a>
                                            </li>
                                            <li>
                                                <a href='milaravel.html'>PHP LARAVEL</a>
                                            </li>
                                            <li>
                                                <a href='minet.html'>ASP.NET</a>
                                            </li>
                                            <li>
                                                <a href='misymfony.html'>PHP SYMFONY</a>
                                            </li>
                                            <li>
                                                <a href='micodeigniter.html'>PHP CODEIGNITER</a>
                                            </li>
                                            <li>
                                                <a href='mitextoplano.html'>JAVASCRIPT</a>
                                            </li>
                                            <li>
                                                <a href='mijava.html'>JAVA</a>
                                            </li>
                                            <li>
                                                <a href='miotros.html'>OTROS</a>
                                            </li>
                                        </ul>
                                </li>
                           
                                <li class='has-sub'>
                                    <a href='#'>EXPERIENCIA<i class='mdi mdi-chevron-down'></i></a>
                                        <ul>
                                            <li>
                                                <a href='empresas.html'>EMPRESAS</a>
                                            </li>
                                            <li>
                                                <a href='clientes.html'>CLIENTES</a>
                                            </li>
                                        </ul>
                                </li>
                            </ul>
                            
                        <!-- Menu - End -->
                        </div>
                    </div>
                    <div class="black inviewport animated delay4" data-effect="fadeInLeftBig">
                        <div class='cssmenu'>
                        <!-- Menu - Start -->
                            <ul class='menu-ul'><li class='has-sub'>
                                <a href='#'>PREFERIDOS<i class='mdi mdi-chevron-down'></i></a>
                                    <ul>
                                        <li>
                                            <a href='pr_webs.html'>WEBS</a>
                                        </li>
                                        <li>
                                            <a href='pr_libros.html'>lIBROS</a>
                                        </li>
                                        <li>
                                            <a href='pr_tutoriales.html'>TUTORIALES</a>
                                        </li>
                                        <li>
                                            <a href='pr_blogs.html'>BLOGS</a>
                                        </li>
                                        <li>
                                            <a href='pr_eventos.html'>EVENTOS</a>
                                        </li>
                            </ul>
                        </li>
                        <li class='has-sub'>
                            <a href='#'>CODE<i class='mdi mdi-chevron-down'></i></a>
                            <ul>
                                <li>
                                    <a href='code_github.html'>GITHUB</a>
                                </li>
                                <li>
                                    <a href='code_otros.html'>OTROS</a>
                                </li>
                             
                            </ul>
                        </li>
                        <li class='has-sub'>
                            <a href='#'>TITULACIONES <i class='mdi mdi-chevron-down'></i></a>
                            <ul>
                                <li>
                                    <a href='tit_dam.blade.php'>DAM</a>
                                </li>
                                <li>
                                    <a href='tit_daw.blade.php'>DAW</a>
                                </li>
                                <li>
                                    <a href='tit_asir.blade.php'>ASIR</a>
                                </li>
                                <li>
                                    <a href='tit_diplomatura.blade.php'>GESTION Y ADMON. PUBLICA</a>
                                </li>
                                <li>
                                    <a href='tit_symfony.blade.php'>CURSO SYMFONY</a>
                                </li>
                                <li>
                                    <a href='tit_html5.blade.php'>CURSO HTML5</a>
                                </li>
                                <li>
                                    <a href='tit_laravel.blade.php'>CURSO LARAVEL</a>
                                </li>
                                <li>
                                    <a href='tit_aspnet.blade.php'>CURSO ASP.NET</a>
                                </li>
                            </ul>
                        </li>
                        
                    </ul>
                    <!-- Menu - End -->
                </div>
            </div>
                </div>
    </div>
    <!-- Menu Top Bar - End -->
    
    <!-- Logo and Mobile Menu - Start -->
    <div class='header-logo-wrap'>
        <div class="container">
            <div class="logo col-xs-2">
                <span>WEB</span>
            </div>
            <div class="menu-mobile col-xs-10 pull-right cssmenu">
                <i class="mdi mdi-menu menu-toggle"></i>
                <ul class="menu" id='parallax-mobile-menu'>
                </ul>
            </div>
        </div>
    </div>
    <!-- Logo and Mobile Menu - End -->
    <!-- Header Slide - Start -->
    <div class="header-slide" style="position:relative;">
        <img alt="header-banner-image" src="img/rub.jpg" class='header-img' style=''>
        <div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
            <!-- Header Text - Start -->
            <div class="maintext">
                <div class="primary-text inviewport animated delay4" data-effect="fadeInRightBig">
                    <div class="left-line">
                        <h4>Bienvenido a mi</h4>
                        <h1>porta</h1>
                    </div>
                </div>
                <div class="black-text inviewport animated delay4" data-effect="fadeInLeftBig">
                    <div>
                        <h1>folio</h1>
                    </div>
                </div>
            </div>
            <!-- Header Text - End -->
        </div>
        
    </div>
     
    
    <!-- Header Slide - End -->
    
</section>
<!-- Section End - Header -->

<!-- Section start - text -->
<section class=' padding-top-50 padding-bottom-25 ' >
    <!-- Angled Section - Start -->
    <div class="angled_down_inside white">
        <div class="slope upleft"></div>
        <div class="slope upright"></div>
    </div>
        <!-- Angled Section - End -->
        
    <div class="container">
    
    <h4>COMO SOY:</h4>
<p class="subheading"> 
Soñador, valiente, apasionado, perseverante y obstinado. En busca de aquello que me hace sentir bien en todos los aspectos de la vida. </p>
<h4>MIS OBJETIVOS:</h4>
<p class="subheading"> Conocer y dominar todo lo que tiene que ver con el mundo de la web y el avance tecnológico, buscando sorprender con cada trabajo y huir de convencionalismos. </p>
<h4>PRESENTE Y FUTURO:</h4>
<p class="subheading"> Dejarme llevar por la curiosidad que siempre me invadió dentro del mundo de la computación. Me gustaría que esta energía que siempre me mueve me lleve a innovar dentro de este mundo y que otros sigan mis pasos.</p>
    </div>

</section>
<!-- Section End - text -->
<!-- Section Start - Footer -->
<section class='footer bg-black padding-top-75 padding-bottom-25 '>
<!-- Angled Section - Start -->
<div class="angled_down_inside black">
<div class="slope upleft"></div>
<div class="slope upright"></div>
</div>
<!-- Angled Section - End -->
<div class="container">
<div class="row">
<!-- Text Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-widget inviewport animated delay1" data-effect="fadeInUp">
<div class="logo">
    <span>WEB</span>
</div>
<p>Página creada para que cualquier persona interesada en mi trabajo pueda verlo libremente, se procura un contenido sencillo e intuitivo. Espero que finalmente nos conozcamos.</p>
<p>Email: ronnmad@hotmail.es<br> Tlfno: 667703296</p>
</div>
<!-- 1 Widget - End -->
<!-- 2 Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 twitter-widget inviewport animated delay2" data-effect="fadeInUp">
<h4>Redes sociales</h4>
<div class="headul left-align"></div>
<div class="tweet">
    <i class="mdi mdi-twitter"></i>
    <div class="message"><strong>enlace twitter</strong> texto :)  <small>texto secundario</small></div>
</div>
<div class="facebook">
    <i class="mdi mdi-twitter"></i>
    <div class="message"><strong>enlace facebook</strong> texto :8 <small>texto secundario</small></div>
</div>
</div>
<!-- 2 Widget - End -->
<!-- 3 Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 flickr-widget inviewport animated delay3" data-effect="fadeInUp">
<h4>webs relacionadas</h4>
<div class="headul left-align"></div>
<div class="row">
    
</div>
</div>
<!-- 3 Widget - End -->
</div>
</div>
<!-- Copyright Bar - Start -->
<div class="copyright">
<div class="col-md-12">
<div class="container">
<div class="">
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 message inviewport animated delay1" data-effect="fadeInUp">
        <span class="">&copy; rubenfraguas.com | web developer</span><br/>
        
    </div>
</div>
</div>
</div>
</div>
<!-- Copyright Bar - End -->
</section>
<!-- Section End - Footer -->
</body>
</html>