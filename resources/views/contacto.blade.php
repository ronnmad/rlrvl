<!-- inicio -->
@extends ('template/base')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/zxsp3.jpg')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<div class="bg-overlay "></div>
<div class="container">
<h1 class="heading">Datos de contacto </h1>
<div class="headul"></div>
<div class="col-lg-9 col-md-9 col-xs-12 col-sm-12 inviewport animated delay1" data-effect="fadeInUp">
    <ul class="list-group">
        <li class="list-group-item list-group-item-info info">· Nombre: Ruben Fraguas</li>
        <li class="list-group-item list-group-item-info post">· Email: ronnmad@hotmail.es</li>
        <li class="list-group-item list-group-item-info">· Teléfono: 667703296</li>
        <li class="list-group-item list-group-item-info">· Zona geográfica para reunión: provincia de Madrid</li>
        <li class="list-group-item list-group-item-info">· Trabajo a distancia: modo escritorio remoto/mail/telefóno</li>
    </ul> 
</div>
<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6 contacts inviewport animated delay1" data-effect="fadeInUp">
<img alt='contact-person' src='{{asset(img/ruben2.jpg)}}' class='img-responsive'>
</div>
</div>

@endsection
