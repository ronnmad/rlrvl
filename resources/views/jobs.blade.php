
@extends ('template/baseUsuario')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/job.jpg')}}" class='header-img' style='height: 300px' >
<div class="overlay overlay1">
    <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
    <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<!-- Section Start - Blogs -->
<section class='padding-bottom-0 '>
    <div class="container">
        <h1 class="heading"> JOBS </h1>
        <div class="headul"></div>
        <h3 class="heading" style="color: #58ACFA"> {{$usuario->nombreCompleto}} </h3>
        <hr>
        
        <div id="accordion">
          <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #58ACFA;font-size:25px" >
          <span class='glyphicon glyphicon-menu-down'></span>
          
          <span class='glyphicon glyphicon-list'>&nbsp{{$jobs->count()}}</span>
          </a>
          </h4>
          <div id="collapseOne" class="panel-collapse collapse outside">
            <div class="panel-body">
                <ul> 
            @foreach($jobs as $job)       
                  <li class="list-unstyled"><span class='glyphicon glyphicon-saved'></span>&nbsp &nbsp{{$job->nombre}}</li>
            @endforeach
              </ul>
            </div>
          </div>
        </div>
        <hr>
        @foreach($jobs as $job)  
        <div class="row" >
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" style="background: whitesmoke">
                <h3><a class="btn-link" href="{{$job->enlace}}" >{{$job->nombre}}</a></h3>
                <p><strong>Fecha de Guardado:</strong> {{$job->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                <p>{{$job->descripcion}}</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area">                  
                <img class="img-responsive img-rounded" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$job->imagen)}}" border="1" alt="" width="400" height="300">
            </div>   
        </div>
        <div class="headul left-align"></div>
        @endforeach
        <hr>
        {!! $jobs->render() !!}
    </div>
</section>
@endsection

