<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
    <!-- Admin - Start -->

<div class="row">
<h1 class="heading">ADMINISTRACION DE EXPERIENCIA</h1>
<div class="headul"></div>
<h3 class="heading">{{$usuario->nombreCompleto}}</h3>
<div class='container table-responsive '>
    <a href="{{route ('expeAdmin.create')}}" class='btn-sm btn-info'>
    <span class='glyphicon glyphicon-floppy-disk'> NUEVO </span>
    </a>
<table class='table' >
    <thead>
    <td>EDITAR</td>
    <td>BORRAR</td>    
    <td>ID</td>
    <td>TIPO</td>
    <td >NOMBRE</td>
    <td>ENLACE</td>
    <td>IMAGEN</td>
    <td>DESCRIPCION</td>
    </thead>
    <tbody>
        @foreach($expes as $expe)
        @if($expe->id_user == Auth::id())
        <tr>
        <td>
            <a href="{{route('expeAdmin.edit', $expe->id )}}" class='btn-sm btn-warning '> 
                <span class='glyphicon glyphicon-cog'></span>
            </a>
        </td>
        <td>
            <a href="{{route('expeAdmin.destroy', $expe->id)}}" id="{{$expe->nombre}}"  class='btnPromt btn-sm btn-danger'>
                <span class='glyphicon glyphicon-trash'></span>
            </a>
        </td>
        <td>{{$expe->id}}</td>
        <td>{{$expe->tipo}}</td>
        <td>{{$expe->nombre}}</td>
        <td>{{substr($expe->enlace, 0, 20)}}...</td>
        <td><img src='{{str_replace("www.dropbox", "dl.dropboxusercontent",$expe->imagen)}}' class='img-responsive' style="height: 40px; width: 60px"></td>
        <td>{{substr($expe->descripcion, 0, 20)}}...</td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
</div>
{!! $expes->render() !!}
</div>
<!-- End - Admin -->
@endsection
