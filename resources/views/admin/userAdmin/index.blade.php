<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
    <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
    <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<!-- Admin - Start -->

<div class="row">
    <h1 class="heading">DATOS DE USERS</h1>
    <div class="headul"></div>
    <h3 class="heading">Admin: {{$usuario->nombreCompleto}}</h3>
    <div class='container table-responsive '>

        <table class='table' >
    <thead>
    <td>EDITAR</td>
    <td>BORRAR</td>    
    <td>ID</td>
    <td>TIPO</td>
    <td>NOMBRE</td>
    <td>EMAIL</td>
    <td>CREADO</td>
    <td>MODIFICADO</td>
    </thead>
    <tbody>
        @foreach($users as $u)
   
        <tr>
        <td>
            <a href="{{route('UserAdmin.edit', $u->id )}}" class='btn-sm btn-warning '> 
                <span class='glyphicon glyphicon-cog'></span>
            </a>
        </td>
        <td>
            <a href="{{route('UserAdmin.destroy', $u->id)}}" id="{{$u->name}}"  
               class='btnPromt btn-sm btn-danger'>
                <span class='glyphicon glyphicon-trash'></span>
            </a>
        </td>
        <td>{{$u->id}}</td>
        <td>{{$u->type}}</td>
        <td>{{$u->name}}</td>
        <td>{{$u->email}}</td>
        <td>{{$u->created_at}}</td>
        <td>{{$u->updated_at}}</td>    
        </tr>
       
        @endforeach
    </tbody>
</table>
        
        
        
        
        <!--
                    <a href="{{route('usuarioAdmin.destroy', $usuario->id)}}" id="{{$usuario->nombre}}"  class='btnPromt btn-sm btn-danger'>
                        <span class='glyphicon glyphicon-trash'></span>
                    </a>-->

        
    </div>
</div>
<!-- End - Admin -->
@endsection
