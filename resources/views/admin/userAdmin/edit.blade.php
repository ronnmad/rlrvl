<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
    <!-- Admin - Start -->
<div class="row">
<h1 class="heading">EDITAR DATOS DE USUARIO </h1>
<div class="headul"></div>
<h3 class="heading">{{$user->name}}</h3>
{!! Form::open(['route' => ['UserAdmin.update', $user],'method' =>'PUT']) !!}
<div class='form-group'>
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',$user->name,['class' => 'form-control','required' ] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('tipo', 'Tipo') !!}
         {!! Form::select('type', ['member'=> 'Member','admin'=> 'Admin'],$user->type,['class' => 'form-control inline'] ) !!}
    </div>
    <div>
        {{ Form::hidden('id_user', $usuario->id_user) }}
    </div>
    <div class='form-group'>
        <button type="submit" class="btn-sm btn-info">
            <span class='glyphicon glyphicon-floppy-disk'> REGISTRAR </span>
        </button>
    </div>
   
{!! Form::close() !!}
</div>
<!-- End - Admin -->
@endsection
