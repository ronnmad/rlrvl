<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')

    <!-- Admin - Start -->
<div class="row">
<h1 class="heading">Añadir Jobs</h1>
<h3 class="heading">{{$usuario->nombreCompleto}}</h3>
{!! Form::open(['route' => 'jobAdmin.store', 'method' =>'POST']) !!}
    <div class='form-group'>
        {!! Form::label('nombre', 'Nombre') !!}
        {!! Form::text('nombre', null,['class' => 'form-control','required','maxlength=255','minlength=1'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('enlace', 'Enlace') !!}
        {!! Form::url('enlace', null,['class' => 'form-control', 'required','maxlength=255','minlength=8' ]) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('imagen', 'Imagen') !!}
        {!! Form::url('imagen', null,['class' => 'form-control', 'required','maxlength=1020','minlength=8'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('descripcion', 'Descripcion') !!}
        {!! Form::text('descripcion', null,['class' => 'form-control', 'required','maxlength=1020','minlength=1'] ) !!}
    </div>
    <div>
        {{ Form::hidden('id_user', Auth::id()) }}
    </div>
<!--<input type="text" hidden="hidden" name="id_user" value="{{Auth::id()}}"/>-->
    <div class='form-group'>
        <button type="submit" class="btn-sm btn-info">
            <span class='glyphicon glyphicon-floppy-disk'> REGISTRAR </span>
        </button>
 
    </div>
{!! Form::close() !!}
</div>
<!-- End - Admin -->
@endsection
