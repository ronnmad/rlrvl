<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
    <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
    <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<!-- Admin - Start -->

<div class="row table-responsive">
    <h1 class="heading">ESTADÍSTICAS</h1>
    <div class="headul"></div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h6 class="heading">{{$user->name}}</h6>
            <canvas id="chart-area3"></canvas>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h6 class="heading">Sumas totales</h6>
            <canvas id="chart-area5"></canvas>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h6 class="heading">Máximo Usuarios</h6>
            <canvas id="chart-area4"></canvas>
        </div>
    </div>
    <table class='table table-hover' >
        <thead><h3 class="heading" style="color: #58ACFA">{{($user->name)}}</h3></thead>
        <thead style="background-color: lightgrey">
        <td>SECCION</td>    
        <td>REGISTROS USUARIO</td>
        <td>MEDIA POR USUARIO</td>    
        <td>MAXIMO REGISTRO</td>    

        </thead>
        <tbody>
            <tr>
                <td>JOBS</td>
                <td>{{$jobsusuario}}</td>
                <td>{{$jobsmedia}}</td>
                <td>{{$jobsmax}}</td>
            </tr>

            <tr>
                <td>EXPERIENCIA</td>
                <td>{{$expesusuario}}</td>
                <td>{{$expesmedia}}</td>
                <td>{{$expesmax}}</td>
            </tr>
            <tr>
                <td>PREFERIDOS</td>
                <td>{{$prefesusuario}}</td>
                <td>{{$prefesmedia}}</td>
                <td>{{$prefesmax}}</td>
            </tr>
            <tr>
                <td>TITULACIONES</td>
                <td>{{$titsusuario}}</td>
                <td>{{$titsmedia}}</td>
                <td>{{$titsmax}}</td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <canvas id="chart-area1" height="60"></canvas>
        <canvas id="chart-area2" height="60"></canvas>
    </div>
    
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
    <ul class="list-group">
        <li class="list-group-item list-group-item-info info">
           * Para los campos de url en esta app se pueden incluir los enlaces que proporciona DROPBOX, 
        solo hay que copiarlo al igual que una url de imagen normal, PF se encarga de procesarlo.  
        </li>
        <li class="list-group-item list-group-item-info info">
           * Para incluir la url de cualquier imagen de la red se debe pulsar boton derecho del ratón y elegir la opción:
           "Abrir en otra pestaña", después entramos en ella y copiamos la url que acaba en .jpg, .png, etc...  
        </li>
    </ul> 
    </div>
    
    
    <script>

        var barChartData = {
        labels : ["Jobs", "Experiencia", "Preferidos", "Titulaciones"],
                datasets : [
                {
                fillColor : "#6b9dfa",
                        strokeColor : "#ffffff",
                        highlightFill: "#1864f2",
                        highlightStroke: "#ffffff",
                        data : [{{$jobsusuario}}, {{$expesusuario}}, {{$prefesusuario}}, {{$titsusuario}}]
                },
                {
                fillColor : "grey",
                        strokeColor : "#ffffff",
                        highlightFill : "silver",
                        highlightStroke : "#ffffff",
                        data : [{{$jobsmedia}}, {{$expesmedia}}, {{$prefesmedia}}, {{$titsmedia}}]
                }
                ]
        }
        
        var radarChartData = {
        labels : ["Jobs", "Experiencia", "Preferidos", "Titulaciones"],
                datasets : [
                
                {
                fillColor : "grey",
                        strokeColor : "#ffffff",
                        highlightFill : "silver",
                        highlightStroke : "#ffffff",
                        data : [{{$jobsCount}}, {{$expesCount}}, {{$prefesCount}}, {{$titsCount}}]
                },
                {
                fillColor : "#6b9dfa",
                        strokeColor : "blue",
                        highlightFill: "#1864f2",
                        highlightStroke: "blue",
                        data : [{{$jobsusuario}}, {{$expesusuario}}, {{$prefesusuario}}, {{$titsusuario}}]
                }
                ]
        }
        
        data2 = [
        {
        value: {{$jobsusuario}},
                color: "red",
                label: "Jobs"
        },
        {
        value: {{$expesusuario}},
                color: "blue",
                label: "Experiencia"
        },
        {
        value: {{$titsusuario}},
                color: "green",
                label: "Titulacion"
        },
        {
        value: {{$prefesusuario}},
                color: "yellow",
                label: "Preferencias"
        }


        ];
        data3 = [
        {
        value: {{$jobsmax}},
                color: "red",
                label: "Jobs"
        },
        {
        value: {{$expesmax}},
                color: "blue",
                label: "Experiencia"
        },
        {
        value: {{$titsmax}},
                color: "green",
                label: "Titulacion"
        },
        {
        value: {{$prefesmax}},
                color: "yellow",
                label: "Preferencias"
        }
        ];
        
        var ctx1 = document.getElementById("chart-area1").getContext("2d");
        window.myPie = new Chart(ctx1).Bar(barChartData, {responsive:true});
        var ctx2 = document.getElementById("chart-area2").getContext("2d");
        window.myPie2 = new Chart(ctx2).Line(barChartData, {responsive:true});
        var ctx3 = document.getElementById("chart-area3").getContext("2d");
        window.myPie3 = new Chart(ctx3).PolarArea(data2, {responsive:true});
        var ctx4 = document.getElementById("chart-area4").getContext("2d");
        window.myPie4 = new Chart(ctx4).PolarArea(data3, {responsive:true});
        var ctx5 = document.getElementById("chart-area5").getContext("2d");
        window.myPie5 = new Chart(ctx5).Radar(radarChartData, {responsive:true});
        
    </script>
</div>

<!-- End - Admin -->
@endsection
