<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
    <!-- Admin - Start -->

<div class="row">
<h1 class="heading">ADMINISTRACION DE PREFERIDOS</h1>
<div class="headul"></div>
<h3 class="heading">{{$usuario->nombreCompleto}}</h3>
<div class='container table-responsive '>
    <a href="{{route ('prefeAdmin.create')}}" class='btn-sm btn-info'>
    <span class='glyphicon glyphicon-floppy-disk'> NUEVO </span>
    </a>
<table class='table' >
    <thead>
    <td>EDITAR</td>
    <td>BORRAR</td>    
    <td>ID</td>
    <td>TIPO</td>
    <td>NOMBRE</td>
    <td>ENLACE</td>
    <td>IMAGEN</td>
    <td>DESCRIPCION</td>
    </thead>
    <tbody>
        @foreach($prefes as $prefe)
        @if($prefe->id_user == Auth::id())
        <tr>
        <td>
            <a href="{{route('prefeAdmin.edit', $prefe->id )}}" class='btn-sm btn-warning '> 
                <span class='glyphicon glyphicon-cog'></span>
            </a>
        </td>
        <td>
           <a href="{{route('prefeAdmin.destroy', $prefe->id)}}" id="{{$prefe->nombre}}"  class='btnPromt btn-sm btn-danger'>    
                <span class='glyphicon glyphicon-trash'></span>
            </a>
        </td>
        <td>{{$prefe->id}}</td>
        <td>{{$prefe->tipo}}</td>
        <td>{{$prefe->nombre}}</td>
        <td>{{substr($prefe->enlace, 0, 20)}}...</td>
        <td><img src='{{str_replace("www.dropbox", "dl.dropboxusercontent",$prefe->imagen)}}' class='img-responsive' style="height: 40px; width: 60px"></td>
        <td>{{substr($prefe->descripcion, 0, 20)}}...</td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>

</div>
{!! $prefes->render() !!}
</div>
<!-- End - Admin -->
@endsection
