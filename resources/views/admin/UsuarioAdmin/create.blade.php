<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
    <!-- Admin - Start -->
<div class="row">
<h1 class="heading">USUARIO</h1>
<div class="headul"></div>
<h3 class="heading">{{$name}}</h3>
        
{!! Form::open(['route' => 'usuarioAdmin.store', 'method' =>'POST']) !!}
    <div class='form-group'>
        {!! Form::label('presentacion', 'Presentacion') !!}
        {!! Form::textArea('presentacion', null,['size' => '30x5','class' => 'form-control','required','maxlength=2040','minlength=1','maxlength=2040' ] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('nombreCompleto', 'Nombre') !!}
        {!! Form::text('nombreCompleto', null,['class' => 'form-control','required','maxlength=255','minlength=1'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('profesion', 'Profesion') !!}
        {!! Form::text('profesion', null,['class' => 'form-control','required','maxlength=100','minlength=1'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('faceBook', 'FaceBook') !!}
        {!! Form::url('faceBook', null,['class' => 'form-control', 'required','maxlength=1020','minlength=8' ]) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('linkedIn', 'LinkedIn') !!}
        {!! Form::url('linkedIn', null,['class' => 'form-control', 'required','maxlength=1020','minlength=8' ]) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('otraredsocial', 'Otra Red Social') !!}
        {!! Form::url('otraRedSocial', null,['class' => 'form-control', 'required','maxlength=1020','minlength=8' ]) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('imagen', 'Imagen') !!}
        {!! Form::url('imagen', null,['class' => 'form-control', 'required','maxlength=1020','minlength=1'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('zonageografica', 'Zona Geográfica') !!}
        {!! Form::text('zonaGeografica', null,['class' => 'form-control','required','maxlength=255','minlength=1'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('telefono', 'Teléfono') !!}
        {!! Form::text('telefono', null,['class' => 'form-control','numeric|required','maxlength=100','minlength=1'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null,['class' => 'form-control','required','maxlength=255','minlength=1'] ) !!}
    </div>
    <div class='form-group'>
        {!! Form::label('enlacewebpersonal', 'Enlace Web Personal') !!}
        {!! Form::url('enlaceWebPersonal', null,['class' => 'form-control', 'required','maxlength=1020','minlength=8' ]) !!}
    </div>
    <div>
        {{ Form::hidden('id_user', $user->id) }}
    </div>
    <div class='form-group'>
        <button type="submit" class="btn-sm btn-info">
            <span class='glyphicon glyphicon-floppy-disk'> REGISTRAR </span>
        </button>
 
    </div>
{!! Form::close() !!}
</div>
<!-- End - Admin -->
@endsection
