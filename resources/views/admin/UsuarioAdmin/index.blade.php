<!-- inicio -->
@extends ('template/baseAdmin')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/adminR.png')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
    <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
    <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<!-- Admin - Start -->

<div class="row">
    <h1 class="heading">DATOS DE USUARIO</h1>
    <div class="headul"></div>
    <h3 class="heading">{{$name}}</h3>
    <div class='container table-responsive '>
        @if(!$usuario)
        
        <a href="{{route ('usuarioAdmin.create')}}" class='btn btn-info center-block'>
            <span class='glyphicon glyphicon-floppy-disk'> INTRODUCIR DATOS DE USUARIO POR PIMERA VEZ </span>
        </a>
        
        @endif
        @if($usuario)
        <a href="{{route('usuarioAdmin.edit', Auth::id())}}" class='btn-sm btn-warning '> 
            <span class='glyphicon glyphicon-cog'> CAMBIAR DATOS</span>
        </a>
        <hr>
        
        <ul class='list-group ' >
            <li class="list-group-item list-group-item-info">ID: {{$usuario->id_user}}</li>
            <li class="list-group-item list-group-item-info">PRESENTACIÓN: {{$usuario->presentacion}}</li>
            <li class="list-group-item list-group-item-info">NOMBRE COMPLETO: {{$usuario->nombreCompleto}}</li>
            <li class="list-group-item list-group-item-info">PROFESION: {{$usuario->profesion}}</li>
            <li class="list-group-item list-group-item-warning">FACEBOOK: {{$usuario->faceBook}}</li>
            <li class="list-group-item list-group-item-warning">LINKEDIN: {{$usuario->linkedIn}}</li>
            <li class="list-group-item list-group-item-warning">OTRA RED SOCIAL: {{$usuario->otraRedSocial}}</li>
            <li class="list-group-item list-group-item-info">IMAGEN: <img src='{{str_replace("www.dropbox", "dl.dropboxusercontent",$usuario->imagen)}}' class='img-responsive' style="height: 100px; width: 140px"></li>
            <li class="list-group-item list-group-item-info">ZONA GEOGRAFICA: {{$usuario->zonaGeografica}}</li>
            <li class="list-group-item list-group-item-info">TELÉFONO: {{$usuario->telefono}}</li>
            <li class="list-group-item list-group-item-success">EMAIL: {{$usuario->email}}</li>
            <li class="list-group-item list-group-item-warning">ENLACE WEB PERSONAL: {{$usuario->enlaceWebPersonal}}</li>
        </ul>
    
        <!--
                    <a href="{{route('usuarioAdmin.destroy', $usuario->id)}}" id="{{$usuario->nombre}}"  class='btnPromt btn-sm btn-danger'>
                        <span class='glyphicon glyphicon-trash'></span>
                    </a>-->

        @endif
        
    </div>
</div>
<!-- End - Admin -->









@endsection
