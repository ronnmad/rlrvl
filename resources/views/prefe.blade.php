<!-- inicio -->
@extends ('template/baseUsuario')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/preferido.jpg')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
    <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
    <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<!-- Section Start - Blogs -->
<section class='padding-bottom-0 '>
    <div class="container">
        <h1 class="heading"> PREFERENCIAS </h1>
        <div class="headul"></div>
        <h3 class="heading" style="color: #58ACFA"> {{$usuario->nombreCompleto}} </h3>
        <hr>
        <div id="accordion">
          <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #58ACFA;font-size:25px">
          <span class='glyphicon glyphicon-menu-down'></span>
          
          <span class='glyphicon glyphicon-list'>&nbsp{{$prefes->count()}}</span>
          </a>
          </h4>
          <div id="collapseOne" class="panel-collapse collapse outside">
            <div class="panel-body">
                <ul> 
            @foreach($prefes as $prefe)       
                  <li class="list-unstyled"><span class='glyphicon glyphicon-saved'></span>&nbsp &nbsp{{$prefe->nombre}}</li>
            @endforeach
              </ul>
            </div>
          </div>
        </div>
        <hr>
        
        @foreach($prefes as $prefe)
        @if($prefe->tipo == "web")
        <div id="webs">
            <h2>WEBS.</h2>
            <hr>
            <div class="row" id="{{$prefe->id}}">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" style="background: whitesmoke"> 
                    <h3><a class="btn-link" href="{{$prefe->enlace}}">{{$prefe->nombre}}</a></h3>
                    <p><strong>Fecha de Guardado:</strong> {{$prefe->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                    <p>{{$prefe->descripcion}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" >  
                    <img class="img-responsive img-rounded" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$prefe->imagen)}}" border="1" alt="" width="400" height="300">
                </div>
            </div>
            <div class="headul left-align"></div>
        </div>
        @endif
        <hr>
        @if($prefe->tipo == "libro")
        <div id="libros">
            <h2>LIBRO.</h2>
            <hr>
            <div class="row" id="{{$prefe->id}}">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" style="background: whitesmoke"> 
                    <h3><a class="btn-link" href="{{$prefe->enlace}}">{{$prefe->nombre}}</a></h3>
                    <p><strong>Fecha de Guardado:</strong> {{$prefe->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                    <p>{{$prefe->descripcion}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" >  
                    <img class="img-responsive img-rounded" src="{{$prefe->imagen}}" border="1" alt="" width="400" height="300">
                </div>
            </div>
            <div class="headul left-align"></div>
        </div>
        @endif
        <hr>
        @if($prefe->tipo == "blog")
        <div id="blogs">
            <h2>BLOG.</h2>
            <hr>
            <div class="row" id="{{$prefe->id}}">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" style="background: whitesmoke"> 
                    <h3><a class="btn-link" href="{{$prefe->enlace}}">{{$prefe->nombre}}</a></h3>
                    <p><strong>Fecha de Guardado:</strong> {{$prefe->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                    <p>{{$prefe->descripcion}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" >  
                    <img class="img-responsive img-rounded" src="{{$prefe->imagen}}" border="1" alt="" width="400" height="300">
                </div>
            </div>
            <div class="headul left-align"></div>
        </div>
        @endif
        <hr>
        @if($prefe->tipo == "video")
        <div id="videos">
            <h2>VIDEO.</h2>
            <hr>
            <div class="row" id="{{$prefe->id}}">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" style="background: whitesmoke"> 
                    <h3><a class="btn-link" href="{{$prefe->enlace}}">{{$prefe->nombre}}</a></h3>
                    <p><strong>Fecha de Guardado:</strong> {{$prefe->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                    <p>{{$prefe->descripcion}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" >  
                    <img class="img-responsive img-rounded" src="{{$prefe->imagen}}" border="1" alt="" width="400" height="300">
                </div>
            </div>
            <div class="headul left-align"></div>
        </div>
        @endif
        <hr>
        @if($prefe->tipo == "otro")
        <div id="otros">
            <h2>OTRO SITIO INTERESANTE.</h2>
            <hr>
            <div class="row" id="{{$prefe->id}}">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" style="background: whitesmoke"> 
                    <h3><a class="btn-link" href="{{$prefe->enlace}}">{{$prefe->nombre}}</a></h3>
                    <p><strong>Fecha de Guardado:</strong> {{$prefe->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                    <p>{{$prefe->descripcion}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" >  
                    <img class="img-responsive img-rounded" src="{{$prefe->imagen}}" border="1" alt="" width="400" height="300">
                </div>
            </div>
            <div class="headul left-align"></div>
        </div>
        @endif
        @endforeach
        <hr>
        {!! $prefes->render() !!}
    </div>
</section>
<!-- Section End - Blogs -->
@endsection
