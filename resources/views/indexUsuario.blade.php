<!-- inicio -->
@extends ('template/baseUsuario')
@section('imag')
<img alt="mi foto" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$usuario->imagen)}}" class='header-img' style='height: 700px '>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
            <div class="maintext">
                <div class="primary-text inviewport animated delay4" data-effect="fadeInRightBig">
                    <div class="left-line">
                        <h4>Bienvenido a mi</h4>
                        <h1>porta</h1>
                    </div>
                </div>
                <div class="black-text inviewport animated delay4" data-effect="fadeInLeftBig">
                    <div>
                        <h1>folio</h1>
                    </div>
                </div>
            </div>
</div>
@endsection
@section ('content')

<div class="row">
<h1 class="heading" style="font-size: 60px; font-weight: bolder; color: #58ACFA "> {{$usuario->nombreCompleto}} </h1>
</div>

<div class="divPanel page-content">
        <div class="row-fluid">
            <div class="list_carousel responsive" style="height: auto">
                        <ul id="list_photos">
                            @if((($jobs->count())+($expes->count())+($prefes->count())+($tits->count())) > 4)
                            @foreach($jobs as $job)
                            <li><img title="{{$job->nombre}}" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$job->imagen)}}" class="img-responsive" style="width:100%; height: 150px">  </li>     
                            @endforeach
                            @foreach($expes as $expe)
                            <li><img title="{{$expe->nombre}}" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$expe->imagen)}}" class="img-responsive" style="width:100%; height: 150px">  </li>     
                            @endforeach
                            @foreach($tits as $tit)
                            <li><img title="{{$tit->nombre}}" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$tit->imagen)}}" class="img-responsive" style="width:100%; height: 150px">  </li>     
                            @endforeach
                            @foreach($prefes as $prefe)
                            <li><img title="{{$prefe->nombre}}" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$prefe->imagen)}}" class="img-responsive" style="width:100%; height: 150px">  </li>     
                            @endforeach
                            @endif
                        </ul>
                    </div> 
            </div>
    </div>

<div class="row">
<h2 class="heading"> Presentacion </h2>
<div class="headul"></div>

    <p class="subheading">Hola mi nombre es {{$usuario->nombreCompleto}}. Mi profesión es {{$usuario->profesion}} y suelo prestar mis servicios por {{$usuario->zonaGeografica}} .</p>
    <p class="subheading">{{$usuario->presentacion}}</p>
    <p class="subheading">Si desea ver mi trayectoria a nivel laboral comence a configuarar este portafolio desde el {{$usuario->created_at->format('d \\d\\e\\l m \\d\\e Y')}} , y he dejado información sufuciente en las diferentes secciones de mi Portafolio.</p>
    <p class="subheading">He elegido esta web porque me pareció un buen sitio web donde dejar mi CV digital.</p>
</div>
<script type="text/javascript">$('#list_photos').carouFredSel({ responsive: true, width: '100%', scroll: 1, items: {width: 320,visible: {min: 2, max: 6}} });</script>
@endsection
