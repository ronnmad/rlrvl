
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>CV PF</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/pdf.css')}}" media="all" />
  </head>
  <body>
      <div style="margin-right: 100px">
    <header class="clearfix">
      <div id="logo">
        <img src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$usuario->imagen)}}">
      </div>
      <h1>CV de {{$usuario->nombreCompleto}}</h1>
      <h1><img alt="" src="{{asset('img/trabajo.png')}}" class='header-img' style='height:30px; width:30px'>    {{$usuario->profesion}}</h1>
      <div class="clearfix">
        <h2> <img alt="" src="{{asset('img/persona.png')}}" class='header-img' style='height:25px; width:25px'>  DATOS PERSONALES </h2>
        <div><b>Nombre:</b> {{$usuario->nombreCompleto}}</div>
        <div><b>Dirección:</b> {{$usuario->zonaGeografica}}</div>
        <div><b>Teléfono:</b> {{$usuario->telefono}}</div>
        <div><b>Email:</b> {{$usuario->email}}</div>
        <div><b>Enlace FaceBook: </b><a href="{{$usuario->faceBook}}">{{$usuario->faceBook}}</a></div>
        <div><b>Enlace LinkedIn: </b><a href="{{$usuario->linkedIn}}">{{$usuario->linkedIn}}</a></div>
        <div><b>Enlace Web Personal: </b><a href="{{$usuario->enlaceWebPersonal}}">{{$usuario->enlaceWebPersonal}}</a></div>
        <br>
      </div>
      <hr>
    </header>
    <main>
     <div class="clearfix">
        <h2><img alt="" src="{{asset('img/formacion.png')}}" class='header-img' style='height:30px; width:30px'>  FORMACION</h2>
        @foreach($tits as $key=>$tit)
            <div><b>Titulo {{$key+1}}: </b> {{$tit->nombre}} de tipo:{{$tit->tipo}} , {{$tit->descripcion}}</div>
        @endforeach
        <br>
      </div>
      <hr>
      <div class="clearfix">
        <h2><img alt="" src="{{asset('img/expe.png')}}" class='header-img' style='height:30px; width:30px'>  EXPERIENCIA</h2>
        @foreach($expes as $key=>$expe)
            <div><b>Experiencia {{$key+1}}:</b> {{$expe->nombre}}, {{$expe->descripcion}}</div>
        @endforeach
        <br>
      </div>
      <hr>
      <div class="responsive">
        <h2><img alt="" src="{{asset('img/jobs.png')}}" class='header-img' style='height:30px; width:30px'>  TRABAJOS REALIZADOS</h2>
        @foreach($jobs as $key=>$job)
            <div><b>Trabajo {{$key+1}}:</b> {{$job->nombre}}, {{$job->descripcion}}</div>
        @endforeach
        <br>
      </div>
      <hr>
      <div class="clearfix">
        <h2><img alt="header-banner-image" src="{{asset('img/prefe.png')}}" class='header-img' style='height:30px; width:30px'>   PREFERENCIAS EN LA RED</h2>
        @foreach($prefes as $key=>$prefe)
            <div><b>Preferencia {{$key+1}}:</b> {{$prefe->nombre}} de tipo:{{$prefe->tipo}} , {{$prefe->descripcion}}</div>
        @endforeach
        <br>
      </div>
   
    </main>
    <footer>
        <p><img alt="" src="{{asset('img/archivo.png')}}" class='header-img' style='height:15px; width:15px'>  Datos recopilados de: {{$_SERVER["HTTP_HOST"]}}  con url:{{$_SERVER["PHP_SELF"] }} con fecha {{date('d \\d\\e\\l m \\d\\e Y')}}.
    </footer>
      </div>
  </body>
</html>