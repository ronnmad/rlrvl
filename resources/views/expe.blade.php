<!-- inicio -->
@extends ('template/baseUsuario')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/experiencia.jpg')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
    <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
    <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<!-- Section Start - Blogs -->
<section class='padding-bottom-0 '>
    <div class="container">
        <h1 class="heading"> EXPERIENCIA </h1>
        <div class="headul"></div>
        <h3 class="heading" style="color: #58ACFA"> {{$usuario->nombreCompleto}} </h3>
        <div class="row">
</div>
       <hr> 
       <div id="accordion">
          <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #58ACFA;font-size:25px">
          <span class='glyphicon glyphicon-menu-down'></span>
          
          <span class='glyphicon glyphicon-list'>&nbsp{{$expes->count()}}</span>
          </a>
          </h4>
          <div id="collapseOne" class="panel-collapse collapse outside">
            <div class="panel-body">
                <ul> 
            @foreach($expes as $expe)       
                  <li class="list-unstyled"><span class='glyphicon glyphicon-saved'></span>&nbsp &nbsp{{$expe->nombre}}</li>
            @endforeach
              </ul>
            </div>
          </div>
        </div>
        <hr>
        <div id="empresas">
            <h2>EMPRESAS.</h2>
            <hr>
            @foreach($expes as $expe)
            @if($expe->tipo == "empresas")
            <div class="row" >
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" id="{{$expe->id}}" style="background: whitesmoke">
                    <h3><a class="btn-link" href="{{$expe->enlace}}">{{$expe->nombre}}</a></h3>
                    <p><strong>Fecha de Guardado:</strong> {{$expe->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                    <p>{{$expe->descripcion}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area"> 
                    <img class="img-responsive img-rounded" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$expe->imagen)}}" border="1" alt="" width="400" height="300">
                </div>
            </div>
            <div class="headul left-align"></div>
            @endif
            @endforeach
            <br/><hr>
        </div>

        <div id="clientes">
            <h2>FREELANCE</h2>
            <hr>
            @foreach($expes as $expe)
            @if($expe->tipo == "clientes")
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" id="{{$expe->id}}" style="background: whitesmoke">
                    <h3><a class="btn-link" href="{{$expe->enlace}}">{{$expe->nombre}}</a></h3>
                    <p><strong>Fecha de Guardado:</strong> {{$expe->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                    <p>{{$expe->descripcion}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area">  
                    <img class="img-responsive img-rounded" src="{{$expe->imagen}}" border="1" alt="" width="400" height="300">
                </div>
            </div>
            <div class="headul left-align"></div>
            @endif
            @endforeach
        </div>
        <hr>
        {!! $expes->render() !!}
    </div>

</section>
<!-- Section End - Blogs -->
@endsection
