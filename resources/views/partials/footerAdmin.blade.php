<!-- Footer - Start -->
<div class="container">
<div class="row">
<!-- Text Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-widget animated delay1" data-effect="fadeInUp">
<div class="logo">
    <span>ADMIN</span>
</div>
<p>Zona de Administración del contenido del Portafolio.</p>
<p>Incluir urls de imagenes que funcionen, sino el sitio se vera vacío.</p>
</div>
<!-- 1 Widget - End -->

</div>
</div>
<!-- Copyright Bar - Start -->
<div class="copyright">
<div class="col-md-12">
<div class="container">
<div class="">
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 message animated delay1">
        <span class="">&copy; rubenfraguas.com | web developer</span><br/>
    </div>
</div>
</div>
</div>
</div>
<!-- Copyright Bar - End -->
<!-- Footer - End -->