<!-- Footer - Start -->
<div class="container">
<div class="row">
<!-- Text Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-widget animated delay1" data-effect="fadeInUp">
<h4>Web Portafolio</h4>
<div class="headul white left-align"></div>
<p>Página creada para que cualquier persona interesada en mi trabajo pueda verlo libremente, se procura un contenido sencillo e intuitivo. Espero que finalmente nos conozcamos.</p>
<p>Email: ronnmad@hotmail.es<br> Tlfno: 667703296</p>
</div>
<!-- 1 Widget - End -->
<!-- 2 Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 twitter-widget animated delay2" data-effect="fadeInUp">
<h4>Redes sociales</h4>
<div class="headul white left-align"></div>
<div>
    <a href="https://www.linkedin.com/in/ruben-fraguas-iglesias-2611b644/"><img src="{{asset('img/in.ico')}}"></a>
    <div class="message"><strong>linkedIn </strong><small> red socio-laboral</small></div>
</div>
<div>
    <a href="https://www.facebook.com/ruben.fraguasiglesias"><img src="{{asset('img/f.ico')}}"></a>
    <div class="message"><strong>facebook </strong><small> red social</small></div>
</div>
</div>
<!-- 2 Widget - End -->
<!-- 3 Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 flickr-widget animated delay3" data-effect="fadeInUp">
<h4>webs relacionadas</h4>
<div class="headul white left-align"></div>
<div class="row">
    <ul>
        <li><a class="btn-link" href="http://desarrolloweb.com">desarrollo.web</a></li>
        <li><a class="btn-link" href="https://code.org/">code.org</a></li>
        <li><a class="btn-link" href="http://wedevelopers.com/">wedevelopers</a></li>
    </ul>
   
</div>
</div>
<!-- 3 Widget - End -->
</div>
</div>
<!-- Copyright Bar - Start -->
<div class="copyright">
<div class="col-md-12">
<div class="container">
<div class="">
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 message inviewport animated delay1">
        <span class="">&copy; rubenfraguas.com | web developer</span><br/>
    </div>
</div>
</div>
</div>
</div>
<!-- Copyright Bar - End -->
<!-- Footer - End -->