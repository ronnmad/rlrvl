<!-- nav - Start -->
<!-- Menu Top Bar - Start -->
<div class="topbar " data-effect="fadeIn">
    <div class="menu">
        <div class="primary inviewport animated delay4" data-effect="fadeInRightBig">
            <div class='cssmenu'>
                <!-- Menu - Start -->
                <ul class='menu-ul'>
                    <li class='has-sub'>    
                        <a href='{{asset('/')}}' >PF</a>
                    </li>
                    <li class='has-sub'>    
                        <a href='{{asset(route('indexUsuario',$name))}}' >INICIO </a>
                    </li>
                    @if (Auth::guard())
                    <li><a href="{{asset('admin')}}">ADMIN</a></li>
                    @else
                    <li class='has-sub'>
                        <a href='{{asset('admin/auth/login')}}'>ADMIN</a>
                    </li>
                    @endif
                    <li class='has-sub'>
                        <a href="{{asset(route('contactoUsuario',$name)) }}">CONTACTO</a>
                    </li>
                    <li class='has-sub'>
                        <a href='{{asset(route('jobs',$name))}}'>JOBS<i class='has-sub'></i></a>
                    </li>
                    <li class='has-sub'>
                        <a href='{{asset(route('expe',$name))}}'>EXPERIENCIA<i class='mdi mdi-chevron-down'></i></a>
                        <ul>
                            <li>
                                <a class='enlace' href='{{asset(route('expe#empresas',$name))}}'>EMPRESAS</a>
                            </li>
                            <li>
                                <a class='enlace' href='{{asset(route('expe#clientes',$name))}}'>FREELANCE</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <!-- Menu - End -->
            </div>
        </div>
        <div class="black inviewport animated delay4" data-effect="fadeInLeftBig">
            <div class='cssmenu'>
                <!-- Menu - Start -->
                <ul class='menu-ul'><li class='has-sub'>
                        <a href='{{asset(route('prefe',$name))}}'>PREFERIDOS<i class='mdi mdi-chevron-down'></i></a>
                        <ul>
                            <li>
                                <a class='enlace' href='{{asset(route('prefe#webs',$name))}}'>WEBS</a>
                            </li>
                            <li>
                                <a class='enlace' href='{{asset(route('prefe#videos',$name))}}'>VIDEOS</a>
                            </li>
                            <li>
                                <a class='enlace' href='{{asset(route('prefe#blogs',$name))}}'>BLOGS</a>
                            </li>
                            <li>
                                <a class='enlace' href='{{asset(route('prefe#libros',$name))}}'>LIBROS</a>
                            </li>
                            <li>
                                <a class='enlace' href='{{asset(route('prefe#otros',$name))}}'>OTROS</a>
                            </li>
                        </ul>
                    </li>
                    <li class='has-sub'>
                        <a href='{{asset(route('tit',$name))}}'>TITULACIONES</a>
                    </li>
                    <li class='has-sub'>    
                        <a href='{{asset(route('pdf',$name))}}' >PDF</a>
                    </li>
                </ul>
                <!-- Menu - End -->
            </div>
        </div>
    </div>
</div>
<!-- Menu Top Bar - End -->

<!-- Logo and Mobile Menu - Start -->
<div class='header-logo-wrap'>
    <div class="container">
        <div class="logo col-xs-2">
            <span>PF</span>
        </div>
        <div class="menu-mobile col-xs-10 pull-right cssmenu">
            <i class="mdi mdi-menu menu-toggle"></i>
            <ul class="menu" id='parallax-mobile-menu'>
            </ul>
        </div>
    </div>
</div>
<!-- Logo and Mobile Menu - End -->
<!-- Header Slide - Start -->
<div class="header-slide" style="position:relative;">
    @yield ('imag') 
</div>


<!-- Header Slide - End -->
<!-- nav - End -->