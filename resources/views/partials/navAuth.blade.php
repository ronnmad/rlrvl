<!-- nav - Start -->
<!-- Menu Top Bar - Start -->
            <div class="topbar " data-effect="fadeIn">
                <div class="menu">
                    <div class="primary inviewport animated delay4" data-effect="fadeInRightBig">
                        <div class='cssmenu'>
                            <!-- Menu - Start -->
                            <ul class='menu-ul'>
                                <li class='has-sub'>    
                                    <a href='{{asset('admin/auth/logout')}}'
                                    onclick="
                                    event.preventDefault(); 
                                    document.getElementById('logout-form').submit();">logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    {{ csrf_field() }}
                                    </form>    
                                    
                                </li>
                                
                            </ul>
                        <!-- Menu - End -->
                        </div>
                    </div>
                    <div class="black inviewport animated delay4" data-effect="fadeInLeftBig">
                        <div class='cssmenu'>
                            <!-- Menu - Start -->
                            <ul class='menu-ul'><li class='has-sub'>
                                <li class='has-sub'>    
                                    <a href='{{asset('login')}}' >Login</a>
                                </li>
                                <li class='has-sub'>    
                                    <a href='{{asset('register')}}' >Registrarse</a>
                                </li>
                            </ul>
                            <!-- Menu - End -->
                        </div>
                    </div>
                </div>
            </div>
    <!-- Menu Top Bar - End -->
    
    <!-- Logo and Mobile Menu - Start -->
    <div class='header-logo-wrap'>
        <div class="container">
            <div class="logo col-xs-2">
                <span>LOGIN</span>
            </div>
            <div class="menu-mobile col-xs-10 pull-right cssmenu">
                <i class="mdi mdi-menu menu-toggle"></i>
                <ul class="menu" id='parallax-mobile-menu'>
                </ul>
            </div>
        </div>
    </div>
    <!-- Logo and Mobile Menu - End -->
    <!-- Header Slide - Start -->
    <div class="header-slide" style="position:relative;">
        @yield ('imag')
    </div>
     
    
    <!-- Header Slide - End -->
    <!-- nav - End -->