<!-- Footer - Start -->
<div class="container">
<div class="row">
<!-- Text Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-widget animated delay1" data-effect="fadeInUp">
<h4>Web Portafolio</h4>
<div class="headul white left-align"></div>
<p>Página creada con el objetivo de dar un sitio donde los usuarios puedan tener un CV digital, se procura un contenido sencillo e intuitivo.</p>
<p> webmaster: Rubén Fraguas </p>
</div>
<!-- 1 Widget - End -->
<!-- 2 Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 twitter-widget animated delay2" data-effect="fadeInUp">
<h4>Redes sociales PF</h4>
<div class="headul white left-align"></div>
<div>
    <a href="https://www.linkedin.com/in/ruben-fraguas-iglesias-2611b644/"><img src="{{asset('img/in.ico')}}"></a>
    <div class="message"><strong>linkedIn </strong><small> red socio-laboral</small></div>
</div>
<div>
    <a href="https://www.facebook.com/ruben.fraguasiglesias"><img src="{{asset('img/f.ico')}}"></a>
    <div class="message"><strong>facebook </strong><small> red social</small></div>
</div>
</div>
<!-- 2 Widget - End -->
<!-- 3 Widget - Start -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 flickr-widget animated delay3" data-effect="fadeInUp">
<h4>webs relacionadas</h4>
<div class="headul white left-align"></div>
<div class="row">
    <ul>
        <li><a class="btn-link" href="https://www.portfoliobox.net/es">portafoliobox</a></li>
        <li><a class="btn-link" href="https://www.flipsnack.com/es/digital-portfolio/">flipsnack</a></li>
        <li><a class="btn-link" href="https://www.soyfreelancer.com/blog/emprendedurismo/crear-portafolios-cuando-no-tienes-experiencia/">soyfreelancer</a></li>
    </ul>
   
</div>
</div>
<!-- 3 Widget - End -->
</div>
</div>
<!-- Copyright Bar - Start -->
<div class="copyright">
<div class="col-md-12">
<div class="container">
<div class="">
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 message inviewport animated delay1">
        <span class="">&copy; rubenfraguas.com | web developer</span><br/>
    </div>
</div>
</div>
</div>
</div>
<!-- Copyright Bar - End -->
<!-- Footer - End -->