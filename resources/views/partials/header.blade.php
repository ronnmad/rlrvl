<!-- header-start -->
<title>Portfolio ruben</title>
<meta charset="utf-8"/>
<meta name=viewport content="width=device-width, initial-scale=1">

<!--STYLESHEETS -->
<link rel="stylesheet" type="text/css" href="{{asset('css/animate.caliber.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/materialdesignicons.caliber.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
<!--        <link rel = "stylesheet" href = "http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('css/style5.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}" >
<link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.css')}}" >
<link rel="stylesheet" type="text/css" href="{{asset('css/owl.transitions.css')}}" >
<link rel="stylesheet" id="main-style" type="text/css" href="{{asset('css/style_1.css')}}">

  
<!--SCRIPTS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('js/pace.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/inviewport-1.3.2.js')}}"></script>
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('js/Chartr.js')}}"></script>
<script type="text/javascript" src="{{asset('js/Chart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.carouFredSel-6.2.0-packed.js')}}"></script>

<!--Mixitup -->
<!--<script type="text/javascript" src="{{asset('js/jquery.mixitup.min.js')}}"></script>-->
<!--Fancybox -->
<!--<script type="text/javascript" src="{{asset('js/jquery.fancybox.pack.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.fancybox.css')}}" media="screen" />-->

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- header-end -->


<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{asset('js/m.js')}}"></script>
