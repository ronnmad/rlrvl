<!DOCTYPE html>
<html>
    <head>
        @include ('partials.header')
        
    </head>
    <body class=" angled  yellow">
        <!-- Section Start - Header -->
        <section class="header bg-lightgray header-1" >
            @include ('partials.navAdmin')
            @include ('partials.anglewhite')
        </section>
        <!-- Section End - Header -->
        <!-- Section start - text --> 
        <section class='section-heading padding-top-25 padding-bottom-100 ' >
            <div class='container'>
                @include('flash::message')
                @yield ('content') 
            </div>
        </section>      
        <!-- Section End - text -->
        <!-- Section Start - Footer -->
        <section class='footer bg-black padding-top-75 padding-bottom-25 '>
            @include ('partials.angleblack')
            @include ('partials.footerAdmin')
        </section>
        <!-- Section End - Footer -->
        @include ('partials.confirm');
</body>
</html>