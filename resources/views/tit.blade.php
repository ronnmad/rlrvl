<!-- inicio -->
@extends ('template/baseUsuario')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/titulacion.jpg')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
    <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
    <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<!-- Section Start - Blogs -->
<section class='padding-bottom-0 '>
    <div class="container">
        <h1 class="heading"> TITULACIONES </h1>
        <div class="headul"></div>
        <h3 class="heading" style="color: #58ACFA"> {{$usuario->nombreCompleto}} </h3>
        <hr>
        <div id="accordion">
          <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #58ACFA;font-size:25px">
          <span class='glyphicon glyphicon-menu-down'></span>
          <span class='glyphicon glyphicon-list'>&nbsp{{$tits->count()}}</span>
          </a>
          </h4>
          <div id="collapseOne" class="panel-collapse collapse outside">
            <div class="panel-body">
                <ul> 
            @foreach($tits as $tit)
                  <li class="list-unstyled"><span class='glyphicon glyphicon-saved'></span>&nbsp &nbsp{{$tit->nombre}}</li>
            @endforeach
              </ul>
            </div>
          </div>
        </div>
        <hr>
        
        @foreach($tits as $tit)
        <div class="row" id="{{$tit->id}}">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" style="background: whitesmoke">
                <h3><a class="btn-link" href="{{$tit->enlace}}">{{$tit->nombre}}</a></h3>
                <p><strong>Fecha de Guardado:</strong> {{$tit->created_at->format('d \\d\\e\\l m \\d\\e Y')}} </p>
                <p><strong>Tipo:</strong> {{$tit->tipo}}</p>
                <p>{{$tit->descripcion}}</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area" >  
                <img class="img-responsive img-rounded" src="{{str_replace("www.dropbox", "dl.dropboxusercontent",$tit->imagen)}}" border="1" alt="" width="400" height="300">
            </div>
        </div>
        <div class="headul left-align"></div>
        @endforeach
        <br/>
        {!! $tits->render() !!}
    </div>
</section>
<!-- Section End - Blogs -->
@endsection
