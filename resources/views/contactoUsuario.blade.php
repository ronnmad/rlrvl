<!-- inicio -->
@extends ('template/baseUsuario')
@section('imag')
<img alt="header-banner-image" src="{{asset('img/contacto.jpg')}}" class='header-img' style='height: 300px'>
<div class="overlay overlay1">
            <div class="black inviewport animated delay4" data-effect="fadeInLeftOpacity"></div>
            <div class="primary inviewport animated delay4" data-effect="fadeInRightOpacity"></div>
</div>
@endsection
@section ('content')
<div class="bg-overlay "></div>
<div class="container">
<h1 class="heading">Datos de contacto </h1>
<div class="headul"></div>
<div class="col-lg-9 col-md-9 col-xs-12 col-sm-12 inviewport animated delay1" data-effect="fadeInUp">
    <ul class="list-group ">
        <li class="list-group-item  list-group-item-warning info">· Nombre: {{$usuario->nombreCompleto}} </li>
        <li class="list-group-item  list-group-item-info info">· Profesión: {{$usuario->profesion}} </li>
        <li class="list-group-item  list-group-item-info post">· Email: {{$usuario->email}} </li>
        <li class="list-group-item  list-group-item-info info">· Teléfono: {{$usuario->telefono}} </li>
        <li class="list-group-item  list-group-item-info info">· Zona geográfica: {{$usuario->zonaGeografica}} </li>
        <li class="list-group-item  list-group-item-info url">· <a class="btn-link" href="{{$usuario->faceBook}}"> Mi FaceBook </a> </li>
        <li class="list-group-item  list-group-item-info url">· <a class="btn-link" href="{{$usuario->linkedIn}}"> Mi LinkedIn </a></li>
        <li class="list-group-item  list-group-item-info url">· <a class="btn-link" href="{{$usuario->otrRedSocial}}"> Otra Red Social </a></li>
        <li class="list-group-item  list-group-item-info url">· <a class="btn-link" href="{{$usuario->enlaceWebPersonal}}"> Web Personal </a> </li>
    </ul> 
</div>
<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6 contacts inviewport animated delay1" data-effect="fadeInUp">
    <img src='{{str_replace("www.dropbox", "dl.dropboxusercontent",$usuario->imagen)}}' class='img-responsive img-rounded' style="height: 200px; width: 200px">
</div>
</div>

@endsection
