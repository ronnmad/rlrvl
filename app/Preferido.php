<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preferido extends Model
{
    protected $table = "preferidos";
    //para get
    protected $fillable = [
        'id', 'tipo', 'nombre',
        'enlace', 'descripcion','imagen','id_user'
    ];
    protected $hidden = [
        'remember_token','created_at','updated_at'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
