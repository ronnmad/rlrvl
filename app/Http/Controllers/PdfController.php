<?php

namespace App\Http\Controllers;
use App\User;
use App\Usuario;
use App\Job;
use App\Experiencia;
use App\Preferido;
use App\Titulacion;
use Illuminate\Http\Request;


class PdfController extends Controller
{
        public function invoice($name) 
    {
        $id = User::where('name', $name)->value('id');
        $usuario = Usuario::where('id_user', $id)->first();
        $jobs = Job::where('id_user',$id)->orderBy('id','ASC')->paginate(20);
        $expes = Experiencia::where('id',$id)->orderBy('id','ASC')->paginate(20);
        $prefes = Preferido::where('id_user',$id)->orderBy('tipo','ASC')->paginate(20);
        $tits = Titulacion::where('id_user', $id)->orderBy('tipo', 'ASC')->paginate(20);
        $view =  \View::make('pdf', compact('name','user', 'usuario', 'jobs', 'expes', 'prefes', 'tits' ))->render();
        $pdf = \App::make('dompdf.wrapper');
        
        $pdf->loadHtml($view);
        return $pdf->stream('pdf');
    }
 

}
