<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Usuario;
use App\Job;
use App\Experiencia;
use App\Preferido;
use App\Titulacion;
use App\User;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class portafolioController extends Controller {

    //index pf
    public function index() {
        $usuarios = Usuario::all();
        $users = User::all();
        $id = 0;
        return view('index')
                        ->with('users', $users)
                        ->with('id', $id)
                        ->with('usuarios', $usuarios);
    }

    //conacto pf
    public function contacto() {
        return view('contacto');
    }  
    
    //indexSelect
    public function indexSelect(Request $request) {
        
        $id = $request[('usuario')];
        if($id == 0) return redirect()->route('index');
        $user = User::where('id', $id)->first();
        return redirect()->route('indexUsuario', $user->name);
    }

    //index Usuario
    public function indexUsuario($name) {

        $user = User::where('name', $name)->first();
        $usuario = Usuario::where('id_user', $user->id)->first();
        $tits = Titulacion::where('id_user', $user->id)->paginate(50);
        $prefes = Preferido::where('id_user', $user->id)->paginate(50);
        $expes = Experiencia::where('id_user', $user->id)->paginate(50);
        $jobs = Job::where('id_user', $user->id)->paginate(50);
        
//        Flash::info("Este es el Portafolio de " . $usuario->nombreCompleto. ", visite cada sección para ver experiencia y demás información.");
     
        return view('indexUsuario')//['usuario' => $usuario]);
                        ->with('usuario', $usuario)
                        ->with('user', $user)
                        ->with('tits', $tits)
                        ->with('expes', $expes)
                        ->with('jobs', $jobs)
                        ->with('prefes', $prefes)
                        ->with('name', $name);
    }
    
    //contacto Usuario
    public function contactoUsuario($name) {
        $id = User::where('name', $name)->value('id');
        $usuario = Usuario::where('id_user', $id)->first();
        return view('contactoUsuario')
                        ->with('usuario', $usuario)
                        ->with('name', $name);
    }

    //controlador de portafolio
    public function job($name) {
        $id = User::where('name', $name)->value('id');
        $jobs = Job::where('id_user',$id)->orderBy('id','ASC')->paginate(12);
        $usuario = Usuario::where('id_user', $id)->first();
        return view('jobs')
            ->with('jobs', $jobs)
            ->with('name', $name)
            ->with('usuario', $usuario);
    }

    //controlador de portafolio
    public function expe($name) {
        $id = User::where('name', $name)->value('id');
        $expes = Experiencia::where('id_user',$id)->orderBy('id','ASC')->paginate(12);
        $usuario = Usuario::where('id_user', $id)->first();
        return view('expe')
                        ->with('expes', $expes)
                        ->with('usuario', $usuario)
                        ->with('name', $name);
    }

    //controlador de portafolio
    public function expe_empresas($name) {
        $id = User::where('name', $name)->value('id');
        $usuario = Usuario::where('id_user', $id)->first();
        $expes = Experiencia::where('id_user',$id)->orderBy('id','ASC')->paginate(12);

        return view('expe#empresas')
                        ->with('expes', $expes)
                        ->with('usuario', $usuario)
                        ->with('name', $name);
    }
    
    
    //controlador de portafolio
    public function expe_clientes($name) {
        $id = User::where('name', $name)->value('id');
        $usuario = Usuario::where('id_user', $id)->first();
        $expes = Experiencia::where('id_user',$id)->orderBy('id','ASC')->paginate(12);

        return view('expe#clientes')
                        ->with('expes', $expes)
                        ->with('usuario', $usuario)
                        ->with('name', $name);
    }
    
    //controlador de portafolio
    public function prefe($name) {
        $id = User::where('name', $name)->value('id');
        $usuario = Usuario::where('id_user', $id)->first();
        $prefes = Preferido::where('id_user',$id)->orderBy('id','ASC')->paginate(12);

        return view('prefe')
                        ->with('prefes', $prefes)
                        ->with('usuario', $usuario)
                        ->with('name', $name);
    }

    //controlador de portafolio
    public function tit($name) {
        $id = User::where('name', $name)->value('id');
        $usuario = Usuario::where('id_user', $id)->first();
        $tits = Titulacion::where('id_user', $id)->orderBy('id', 'ASC')->paginate(12);

        return view('tit')
                        ->with('tits', $tits)
                        ->with('usuario', $usuario)
                        ->with('name', $name);
    }

//    public function welcome() {
//        return view('welcome');
//    }

    public function login() {
        return view('Auth.login');
    }

    //controlador de portafolio
    public function admin() {
        
        if(Usuario::where('id_user', Auth::id())->count() == 0) Flash::success("Debe introducir datos en apartado Usuario.");
        $numUsers = Usuario::distinct()->get(['id_user'])->count();
        $users = User::all();
        $jobsmax = 0;
        $expesmax = 0;
        $prefesmax = 0;
        $titsmax = 0;
        $jobsCount = Job::count()  ;
        $expesCount = Experiencia::count();
        $prefesCount = Preferido::count();
        $titsCount = Titulacion::count();
        
        
        
        foreach ($users as $us) {
            $jobmax = Job::where('id_user', $us->id)->count();
            if ($jobsmax < $jobmax) {
                $jobsmax = $jobmax;
            }
            $expemax = Experiencia::where('id_user', $us->id)->count();
            if ($expesmax < $expemax) {
                $expesmax = $expemax;
            }
            $prefemax = Preferido::where('id_user', $us->id)->count();
            if ($prefesmax < $prefemax) {
                $prefesmax = $prefemax;
            }
            $titmax = Titulacion::where('id_user', $us->id)->count();
            if ($titsmax < $titmax) {
                $titsmax = $titmax;
            }
        }
        //media    
        if ($numUsers != 0) {
            $jobsmedia = $jobsCount / $numUsers;
            $expesmedia = $expesCount / $numUsers;
            $prefesmedia = $prefesCount  / $numUsers;
            $titsmedia = $titsCount / $numUsers;
            $jobsmedia = number_format($jobsmedia, 2, '.', ',');
            $expesmedia = number_format($expesmedia, 2, '.', ',');
            $prefesmedia = number_format($prefesmedia, 2, '.', ',');
            $titsmedia = number_format($titsmedia, 2, '.', ',');
        } else {
            $jobsmedia = 0;
            $expesmedia = 0;
            $prefesmedia = 0;
            $titsmedia = 0;
        }

        //JOBS
        $jobsusuario = Job::where('id_user', Auth::id())->count();
        $expesusuario = Experiencia::where('id_user', Auth::id())->count();
        $prefesusuario = Preferido::where('id_user', Auth::id())->count();
        $titsusuario = Titulacion::where('id_user', Auth::id())->count();
        $user = User::where('id', Auth::id())->first();
        
        $usuario = Usuario::where('id_user', Auth::id())->first();
 
        return view('admin.admin')
                        ->with('jobsusuario', $jobsusuario)
                        ->with('jobsmedia', $jobsmedia)
                        ->with('jobsmax', $jobsmax)
                        ->with('jobsCount', $jobsCount)
                        ->with('expesusuario', $expesusuario)
                        ->with('expesmedia', $expesmedia)
                        ->with('expesmax', $expesmax)
                        ->with('expesCount', $expesCount)
                        ->with('prefesusuario', $prefesusuario)
                        ->with('prefesmedia', $prefesmedia)
                        ->with('prefesmax', $prefesmax)
                        ->with('prefesCount', $prefesCount)
                        ->with('titsusuario', $titsusuario)
                        ->with('titsmedia', $titsmedia)
                        ->with('titsmax', $titsmax)
                        ->with('titsCount', $titsCount)
                        ->with('usuario', $usuario)
                        ->with('user', $user);
    }

}
