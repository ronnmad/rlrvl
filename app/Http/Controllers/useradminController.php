<?php

namespace App\Http\Controllers;
use App\User;
use App\Usuario;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class useradminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $users = User::all();
        $user = User::where('id',$id)->first();
        $name = User::where('id',$id)->first();
        return view('admin.UserAdmin.index')
                        ->with('users', $users)
                        ->with('user', $user)
                        ->with('name', $name)
                        ->with('usuario',$usuario);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($idtit) {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$idtit)->first();

        return view('admin.UserAdmin.edit')
                        ->with('user', $user)
                        ->with('usuario',$usuario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $user = User::find($id);
        $user->fill($request->all());
        $user->save();
        Flash::success("Se ha actualizado  " .$user->name );
        return redirect()->route('UserAdmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $user = User::find($id);
        $user->delete();
        Flash::error("Se ha borrado  " . $user->name);
        return redirect()->route('UserAdmin.index');
    }

}

