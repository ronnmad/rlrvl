<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Preferido;
use App\Usuario;
use App\User;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;

class PrefeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $id = Auth::id();
        $prefes = Preferido::where('id_user',$id)->paginate(5);
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        if($usuario == null)
        {
            return redirect()->route('admin.admin');
        }
        else
        {
        return view('admin.prefeAdmin.index')
                ->with('prefes', $prefes)
                ->with('user', $user)
                ->with('usuario',$usuario);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        return view('admin.prefeAdmin.create') 
           ->with('user', $user)
           ->with('usuario',$usuario);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {

        $prefe = new Preferido($request->all());
        $prefe->save();
        Flash::success("Se ha registrado  " . $prefe->nombre);
        return redirect()->route('prefeAdmin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($idprefe) {
        $id=Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        $prefe = Preferido::where('id',$idprefe)->first();
        return view('admin.prefeAdmin.edit')
                        ->with('prefe', $prefe)
                        ->with('user', $user)
                        ->with('usuario', $usuario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $prefe = Preferido::where('id',$id)->first();
        $prefe->fill($request->all());
        $prefe->save();
        Flash::success("Se ha actualizado  " . $prefe->nombre);
        return redirect()->route('prefeAdmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $prefe = Preferido::find($id);
        $prefe->delete();
        Flash::error("Se ha borrado  " . $prefe->nombre);
        return redirect()->route('prefeAdmin.index');
    }

}
