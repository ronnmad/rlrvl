<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Titulacion;
use App\Usuario;
use App\User;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;

class TitController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        $tits = Titulacion::where('id_user',$id)->paginate(5);
        if($usuario == null)
        {
            return redirect()->route('admin.admin');
        }
        else
        {
        return view('admin.titAdmin.index')
                        ->with('tits', $tits)
                        ->with('user', $user)
                        ->with('usuario',$usuario);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
       $id = Auth::id();
       $usuario = Usuario::where('id_user',$id)->first();
       $user = User::where('id',$id)->first();
        return view('admin.titAdmin.create')
           ->with('user', $user)
           ->with('usuario',$usuario);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $tit = new Titulacion($request->all());
        $tit->save();
        Flash::success("Se ha registrado  " . $tit->nombre);
        return redirect()->route('titAdmin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($idtit) {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        $tit = Titulacion::where('id',$idtit)->first();

        return view('admin.titAdmin.edit')
                        ->with('tit', $tit)
                        ->with('user', $user)
                        ->with('usuario',$usuario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $tit = Titulacion::where('id',$id)->first();
        $tit->fill($request->all());
        $tit->save();
        Flash::success("Se ha actualizado  " . $tit->nombre);
        return redirect()->route('titAdmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $tit = Titulacion::find($id);
        $tit->delete();
        Flash::error("Se ha borrado  " . $tit->nombre);
        return redirect()->route('titAdmin.index');
    }

}
