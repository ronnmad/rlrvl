<?php

namespace App\Http\Controllers;
use App\Usuario;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        $name = User::where('id',$id)->value('name');
         if($usuario == null)
        {
            return redirect()->route('usuarioAdmin.create');
        }
        else
        {
        return view('admin/usuarioAdmin/index')
            ->with('usuario', $usuario)
            ->with('user', $user)
            ->with('name', $name);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $id = Auth::id();
        
        $user = User::where('id',$id)->first();
        $name = User::where('id',$id)->value('name');
       
        return view('admin.usuarioAdmin.create')
            ->with('user', $user)
            ->with('name', $name);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $usu = new Usuario($request->all());
        $usu->save();
        Flash::success("Se ha registrado  " . $usu->nombreCompleto);
        return redirect()->route('usuarioAdmin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($idu) {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$idu)->first();
        $user = User::where('id',$idu)->first();
        $name = User::where('id',$idu)->value('name');
        return view('admin.usuarioAdmin.edit')
            ->with('usuario', $usuario)
            ->with('user', $user)
            ->with('name', $name);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $usu = Usuario::find($id);
        $usu->fill($request->all());
        $usu->save();
        Flash::success("Se ha actualizado  " . $usu->nombreCompleto);
        return redirect()->route('usuarioAdmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $usu = Usuario::find($id);
        $usu->delete();
        Flash::error("Se ha borrado  " . $usu->nombreCompleto);
        return redirect()->route('usuarioAdmin.index');
    }
}
