<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Experiencia;
use App\Usuario;
use App\User;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;

class ExpeController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    *  @return \Illuminate\Http\Response
    */
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
   public function index()
   {
        $id = Auth::id();
        $expes = Experiencia::where('id_user',$id)->paginate(5);
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        if($usuario == null)
        {
            return redirect()->route('admin.admin');
        }
        else
        {
        return view('admin.expeAdmin.index')
           ->with('expes',$expes)
           ->with('user', $user)
           ->with('usuario',$usuario);
        }
   }

   /**
    * Show the form for creating a new resource.
    *
    *  @return \Illuminate\Http\Response
    */
   public function create()
   {
       $id = Auth::id();
       $usuario = Usuario::where('id_user',$id)->first();
       $user = User::where('id',$id)->first();
       return view('admin.expeAdmin.create')
          ->with('user', $user)
          ->with('usuario',$usuario);
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $expe = new Experiencia($request->all());
        $expe->save();
        Flash::success("Se ha registrado  ". $expe->nombre);
        return redirect()->route('expeAdmin.index');
    }
    
   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($idexpe){
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        $expe = Experiencia::where('id',$idexpe)->first();
        return view('admin.expeAdmin.edit')
            ->with('user', $user)
            ->with('expe',$expe)
            ->with('usuario',$usuario);     
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function update(Request $request,$id)
    {
    
      $expe = Experiencia::where('id',$id)->first();
      $expe->fill($request->all());
      $expe->save();
      Flash::success("Se ha actualizado  ". $expe->nombre);
      return redirect()->route('expeAdmin.index');
     
    }
   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function destroy($id)
   {
       
       
       $expe = Experiencia::find($id);
       $expe->delete();
       Flash::error("Se ha borrado  ". $expe->nombre);
       return redirect()->route('expeAdmin.index');
       
   }
}