<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Job;
use App\Usuario;
use App\User;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller {
    
    /**
     * Display a listing of the resource.
     *
     *  @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $id = Auth::id();
        $jobs = Job::where('id_user',$id)->paginate(5);
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        if($usuario == null)
        {
            return redirect()->route('admin.admin');
        }
        else
        {
        return view('admin.jobAdmin.index')
                        ->with('jobs', $jobs)
                        ->with('user', $user)
                        ->with('usuario',$usuario);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     *  @return \Illuminate\Http\Response
     */
    public function create() {
        $id = Auth::id();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        return view('admin.jobAdmin.create')
        ->with('user', $user)
        ->with('usuario',$usuario);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $job = new Job($request->all());
        $job->save();
        Flash::success("Se ha registrado Job " . $job->nombre);
        return redirect()->route('jobAdmin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idjob) {
        $id=Auth::id();
        $job = Job::where('id',$idjob)->first();
        $usuario = Usuario::where('id_user',$id)->first();
        $user = User::where('id',$id)->first();
        return view('admin.jobAdmin.edit')
            ->with('job', $job)
            ->with('user', $user)
            ->with('usuario',$usuario); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $job = Job::where('id',$id)->first();
        $job->fill($request->all());
        $job->save();
        Flash::success("Se ha actualizado  " . $job->nombre);
        return redirect()->route('jobAdmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        $job = Job::find($id);
        $job->delete();
        Flash::error("Se ha borrado  " . $job->nombre);
        return redirect()->route('jobAdmin.index');
    }

}
