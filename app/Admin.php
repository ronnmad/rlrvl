<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
     protected $table = "admin";
    //para get
    protected $fillable = [
        'id', 'usuario',
        'password'
    ];
    protected $hidden = [
        'remember_token','created_at','updated_at'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
