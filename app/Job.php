<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
     protected $table = "jobs";
    //para get
    protected $fillable = [
        'id', 'nombre',
        'enlace', 'descripcion','imagen','id_user'
    ];
    protected $hidden = [
        'remember_token','created_at','updated_at',
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
