<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "usuarios";
    
    protected $primaryKey = "id";
    //para get
    protected $fillable = [
        'id','presentacion','nombreCompleto','profesion','faceBook','linkedIn',
        'otraRedSocial','imagen','zonaGeografica','telefono','email','enlaceWebPersonal','id_user'
    ];
    protected $hidden = [
        'remember_token','created_at','updated_at'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}