<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'uses' => 'PortafolioController@index',
    'as'=> 'index'
]);



Auth::routes();

Route::get('contacto',[
    'uses' => 'PortafolioController@contacto',
    'as' => 'contacto'
    
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin',[
    'uses' => 'PortafolioController@admin',
    'as' => 'admin.admin' ])->middleware('auth');

Route::get('/{name}',[
    'uses' => 'PortafolioController@indexUsuario',
    'as' => 'indexUsuario'
]);

Route::post('indexSelect',[
    'uses' => 'PortafolioController@indexSelect',
    'as' => 'indexSelect'

]);

Route::get('{name}/contactoUsuario',[
    'uses' => 'PortafolioController@contactoUsuario',
     'as' => 'contactoUsuario'
]);

Route::get('{name}/jobs',[
    'uses' => 'PortafolioController@job',
    'as' => 'jobs'
]);

Route::get('{name}/expe',[
    'uses' => 'PortafolioController@expe',
    'as' => 'expe'
]);

Route::get('{name}/expe#empresas',[
    'uses' => 'PortafolioController@expe_empresas',
    'as' => 'expe#empresas'
]);

Route::get('{name}/expe#clientes',[
    'uses' => 'PortafolioController@expe_clientes',
    'as' => 'expe#clientes'
]);

Route::get('{name}/prefe',[
    'uses' => 'PortafolioController@prefe',
    'as' => 'prefe'
]);

Route::get('{name}/prefe#webs',[
    'uses' => 'PortafolioController@prefe_webs',
    'as' => 'prefe#webs'
]);

Route::get('{name}/prefe#videos',[
    'uses' => 'PortafolioController@prefe_videos',
    'as' => 'prefe#videos'
]);

Route::get('{name}/prefe#blogs',[
    'uses' => 'PortafolioController@prefe_blogs',
    'as' => 'prefe#blogs'
]);

Route::get('{name}/prefe#libros',[
    'uses' => 'PortafolioController@prefe_libros',
    'as' => 'prefe#libros'
]);

Route::get('{name}/prefe#otros',[
    'uses' => 'PortafolioController@prefe_otros',
    'as' => 'prefe#otros'
]);

Route::get('{name}/pdf',[
    'uses' => 'PdfController@invoice',
    'as' => 'pdf'
]);



Route::get('{name}/tit',[
    'uses' => 'PortafolioController@tit',
    'as' => 'tit'
]);

    Route::group(['prefix' => 'admin'
    ,'middleware'=>'auth'
    ], function(){
    Route::resource('jobAdmin','JobController');   
    Route::get('jobAdmin/{id}/destroy', [
    'uses' => 'jobController@destroy',
    'as' => 'jobAdmin.destroy'
    ]);
    
    Route::resource('expeAdmin', 'ExpeController');
    Route::get('expeAdmin/{id}/destroy', [
    'uses' => 'expeController@destroy',
    'as' => 'expeAdmin.destroy'
    ]);
    
    Route::resource('prefeAdmin', 'PrefeController');
    Route::get('prefeAdmin/{id}/destroy', [
    'uses' => 'prefeController@destroy',
    'as' => 'prefeAdmin.destroy'
    ]);
    
    Route::resource('titAdmin', 'TitController');
    Route::get('titAdmin/{id}/destroy', [
    'uses' => 'titController@destroy',
    'as' => 'titAdmin.destroy'
    ]);
    
    Route::resource('usuarioAdmin', 'UsuarioController');
    Route::get('usuarioAdmin/{id}/destroy', [
    'uses' => 'usuarioController@destroy',
    'as' => 'usuarioAdmin.destroy'
    ]);
    
    Route::resource('UserAdmin', 'useradminController');
    Route::get('UserAdmin/{id}/destroy', [
    'uses' => 'useradminController@destroy',
    'as' => 'UserAdmin.destroy'
    ]);
    
    Route::get('admin', [
    'uses' => 'PortafolioController@admin',
    'as' => 'admin.admin'
    ]);
    
    });
    
    
