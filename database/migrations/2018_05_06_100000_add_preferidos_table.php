<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreferidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->string('nombre',255);
            $table->string('enlace',1020);
            $table->string('imagen',1020);
            $table->string('descripcion',1020);
            $table->integer('id_user')->unsigned();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferidos');
    }
}
