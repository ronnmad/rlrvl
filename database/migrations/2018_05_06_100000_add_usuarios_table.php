<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('presentacion',2040);
            $table->string('nombreCompleto',255);
            $table->string('profesion',100);
            $table->string('faceBook',1020);
            $table->string('linkedIn',1020);
            $table->string('otraRedSocial',1020);
            $table->string('imagen',1020);
            $table->string('zonaGeografica',255);
            $table->string('telefono',100);
            $table->string('email',255);
            $table->string('enlaceWebPersonal',1020);
            $table->integer('id_user')->unsigned();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::drop('usuarios');
    }

}
